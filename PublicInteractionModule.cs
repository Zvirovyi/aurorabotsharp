using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuroraBotSharp.DB;
using Discord;
using Discord.Interactions;
using Discord.WebSocket;

namespace AuroraBotSharp;

public class PublicInteractionModule : InteractionModuleBase<SocketInteractionContext>
{
    private readonly ABDbContext db;
    private readonly BotHandlingService bhs;

    public PublicInteractionModule(ABDbContext db, BotHandlingService bhs)
    {
        this.db = db;
        this.bhs = bhs;
    }

    [Group("settings", "Configuring the bot")]
    public class SettingsGroup : InteractionModuleBase<SocketInteractionContext>
    {
        [Group("saving-roles", "Saving roles feature")]
        public class SavingRolesGroup : InteractionModuleBase<SocketInteractionContext>
        {
            private readonly ABDbContext db;
            private readonly BotHandlingService bhs;

            public SavingRolesGroup(ABDbContext db, BotHandlingService bhs)
            {
                this.db = db;
                this.bhs = bhs;
            }
            
            [SlashCommand("enable", "Enable saving roles feature")]
            public async Task Enable()
            {
                var server = db.Servers.Find(Context.Guild.Id);
                
                if (!server.SaveRoles)
                {
                    server.SaveRoles = true;
                    db.Servers.Update(server);
                    await db.SaveChangesAsync();
                    await bhs.SavedRolesUpdateServerAsync(Context.Guild);
                    await RespondAsync("Successful");
                }
                else
                {
                    await RespondAsync("Already enabled");
                }
            }

            [SlashCommand("disable", "Disable saving roles feature")]
            public async Task Disable()
            {
                var server = db.Servers.Find(Context.Guild.Id);
                
                if (server.AutoRoleID == null)
                {
                    if (server.SaveRoles)
                    {
                        server.SaveRoles = false;
                        db.Servers.Update(server);
                        db.SavedRoles.RemoveRange(db.SavedRoles.AsQueryable()
                            .Where(sr => sr.ServerID == server.ID));
                        await db.SaveChangesAsync();
                        await RespondAsync("Successful");
                    }
                    else
                    {
                        await RespondAsync("Already disabled");
                    }
                }
                else
                {
                    await RespondAsync("Auto-role feature must be disabled");
                }
            }

            [SlashCommand("get", "Get current saving roles feature status")]
            public async Task Get()
            {
                var server = db.Servers.Find(Context.Guild.Id);
                
                await RespondAsync($"Saving roles feature currently {(server.SaveRoles ? "enabled" : "disabled")}");
            }
        }

        [Group("auto-role", "Auto-role feature")]
        public class AutoRoleGroup : InteractionModuleBase<SocketInteractionContext>
        {
            private readonly ABDbContext db;
            private readonly BotHandlingService bhs;

            public AutoRoleGroup(ABDbContext db, BotHandlingService bhs)
            {
                this.db = db;
                this.bhs = bhs;
            }
            
            [SlashCommand("enable", "Enable auto-role feature")]
            public async Task Enable([Summary("role", "auto-role")] IRole role)
            {
                var server = db.Servers.Find(Context.Guild.Id);
                
                if (server.SaveRoles)
                {
                    var roleID = role.Id;
                    if (server.AutoRoleID != roleID)
                    {
                        server.AutoRoleID = roleID;
                        db.Update(server);
                        await db.SaveChangesAsync();
                        await RespondAsync("Successful");
                    }
                    else
                    {
                        await RespondAsync("Auto-role feature already set on this role");
                    }
                }
                else
                {
                    await RespondAsync("Saving roles feature must be enabled");
                }
            }

            [SlashCommand("disable", "Disable auto-role feature")]
            public async Task Disable()
            {
                var server = db.Servers.Find(Context.Guild.Id);
                
                if (server.AutoRoleID != null)
                {
                    server.AutoRoleID = null;
                    db.Servers.Update(server);
                    await db.SaveChangesAsync();
                    await RespondAsync("Successful");
                }
                else
                {
                    await RespondAsync("Already disabled");
                }
            }

            [SlashCommand("get", "Get current auto-role feature status")]
            public async Task Get()
            {
                var server = db.Servers.Find(Context.Guild.Id);
                
                await RespondAsync(
                    $"Auto-role feature currently {(server.AutoRoleID == null ? "disabled" : $"enabled on {Context.Guild.GetRole(server.AutoRoleID.Value).Name} role")}");
            }
        }

        [Group("auto-voice", "Auto-voice feature")]
        public class AutoVoiceGroup : InteractionModuleBase<SocketInteractionContext>
        {
            private readonly ABDbContext db;
            private readonly BotHandlingService bhs;
            private readonly DiscordSocketClient discord;

            public AutoVoiceGroup(ABDbContext db, BotHandlingService bhs, DiscordSocketClient discord)
            {
                this.db = db;
                this.bhs = bhs;
                this.discord = discord;
            }
            
            [SlashCommand("enable", "Enable auto-voice feature")]
            public async Task Enable([Summary("category", "voice channels category")] ICategoryChannel channel,
                [Summary("names", "names list delimited with a comma")]
                string names)
            {
                var server = db.Servers.Find(Context.Guild.Id);

                var newCategory = channel;
                    var newList =
                        names.Split(',')
                        .Select(s => s.Trim()).OrderBy(s => s);
                    if (newList.Any(v => v.Length == 0))
                    {
                        throw new Exception("wrong channel name");
                    }
                    
                    if (server.AutoVoiceCategoryID == null ||
                        server.AutoVoiceCategoryID != newCategory.Id)
                    {
                        server.AutoVoiceCategoryID = newCategory.Id;
                        db.Servers.Update(server);
                    }
                    else
                    {
                        var autoVoiceDisableOldList =
                            db.VoiceChannels.AsQueryable().Where(s => s.ServerID == Context.Guild.Id)
                                .Select(vc => vc.Name).ToList();
                        if (!newList.SequenceEqual(autoVoiceDisableOldList))
                        {
                            db.VoiceChannels.RemoveRange(db.VoiceChannels.AsQueryable()
                                .Where(vc => vc.ServerID == Context.Guild.Id));
                        }
                        else
                        {
                            await RespondAsync("Already enabled with this list");
                            return;
                        }
                    }

                    await db.VoiceChannels.AddRangeAsync(newList.Select(v => new ABVoiceChannel
                    {
                        ServerID = Context.Guild.Id,
                        Name = v,
                        Usages = 0
                    }));
                    await db.SaveChangesAsync();
                    await RespondAsync("Successful");
            }

            [SlashCommand("disable", "Disable auto-voice feature")]
            public async Task Disable()
            {
                var server = db.Servers.Find(Context.Guild.Id);
                
                if (server.AutoVoiceCategoryID != null)
                {
                    server.AutoVoiceCategoryID = null;
                    db.Servers.Update(server);
                    db.VoiceChannels.RemoveRange(db.VoiceChannels.AsQueryable()
                        .Where(vc => vc.ServerID == Context.Guild.Id));
                    await db.SaveChangesAsync();
                    await RespondAsync("Successful");
                }
                else
                {
                    await RespondAsync("Already disabled");
                }
            }

            [SlashCommand("get", "Get current auto-voice feature status")]
            public async Task Get()
            {
                var server = db.Servers.Find(Context.Guild.Id);
                
                if (server.AutoVoiceCategoryID != null)
                {
                    string autoVoiceGetText = String.Join('\n',
                        db.VoiceChannels.AsQueryable().Where(vc => vc.ServerID == Context.Guild.Id)
                            .Select(vc => vc.Name));
                    await RespondAsync(
                        $"Enabled on {((SocketCategoryChannel)discord.GetChannel(server.AutoVoiceCategoryID.Value)).Name}:\n{autoVoiceGetText}");
                }
                else
                {
                    await RespondAsync("Disabled");
                }
            }
        }

        [Group("custom-category", "Custom category feature")]
        public class SettingsCustomCategoryGroup : InteractionModuleBase<SocketInteractionContext>
        {
            private readonly ABDbContext db;
            private readonly BotHandlingService bhs;

            public SettingsCustomCategoryGroup(ABDbContext db, BotHandlingService bhs)
            {
                this.db = db;
                this.bhs = bhs;
            }
            
            [SlashCommand("enable", "Enable custom category feature")]
            public async Task Enable([Summary("category", "voice channels category")] ICategoryChannel channel)
            {
                var server = db.Servers.Find(Context.Guild.Id);

                var newCategory = channel;
                if (server.CustomCategoryID == null ||
                    server.CustomCategoryID != newCategory.Id)
                {
                    server.CustomCategoryID = newCategory.Id;
                    db.Servers.Update(server);
                    await db.SaveChangesAsync();
                    await RespondAsync("Successful");
                }
                else
                {
                    await RespondAsync("Already enabled on this category");
                }
            }

            [SlashCommand("disable", "Disable custom category feature")]
            public async Task Disable()
            {
                var server = db.Servers.Find(Context.Guild.Id);
                
                if (server.CustomCategoryID != null)
                {
                    server.CustomCategoryID = null;
                    db.Servers.Update(server);
                    db.CustomChannels.RemoveRange(db.CustomChannels.AsQueryable()
                        .Where(cc => cc.ServerID == server.ID));
                    await db.SaveChangesAsync();
                    await RespondAsync("Successful");
                }
                else
                {
                    await RespondAsync("Already disabled");
                }
            }

            [SlashCommand("get", "Get current custom category feature status")]
            public async Task Get()
            {
                var server = db.Servers.Find(Context.Guild.Id);
                
                await RespondAsync(
                    $"Custom category feature currently {(server.CustomCategoryID == null ? "disabled" : $"enabled on {Context.Guild.GetCategoryChannel(server.CustomCategoryID.Value).Name} category")}");
            }
        }
    }

    [Group("custom-category", "Custom category feature")]
    public class CustomCategoryGroup : InteractionModuleBase<SocketInteractionContext>
    {
        private readonly ABDbContext db;
        private readonly BotHandlingService bhs;

        public CustomCategoryGroup(ABDbContext db, BotHandlingService bhs)
        {
            this.db = db;
            this.bhs = bhs;
        }
        
        [SlashCommand("create-channel", "Create custom channel")]
        public async Task CreateChannel([Summary("name", "new channel name")] string name,
            [Summary("variant", "channel type")] CustomCategoryChannelVariant variant)
        {
            var createChannelNewName = name.Trim();
            if (createChannelNewName.Length > 0)
            {
                var createChannelServer = await db.Servers.FindAsync(Context.Guild.Id);
                if (createChannelServer.CustomCategoryID != null)
                {
                    var createChannelNewChannel = Context.Guild.CreateTextChannelAsync(
                        createChannelNewName,
                        options =>
                        {
                            options.CategoryId = createChannelServer.CustomCategoryID.Value;
                            options.PermissionOverwrites = new List<Overwrite>
                            {
                                new(Context.User.Id, PermissionTarget.User,
                                    new(manageChannel: PermValue.Allow))
                            };
                        });
                    db.CustomChannels.Add(new()
                    {
                        ServerID = createChannelServer.ID,
                        ID = createChannelNewChannel.Result.Id,
                        OwnerID = Context.User.Id
                    });
                    db.SaveChanges();
                    await RespondAsync("Successful");
                }
                else
                {
                    await RespondAsync("Custom category feature must be enabled");
                }
            }
            else
            {
                throw new Exception("name cannot be empty");
            }
        }
    }
}