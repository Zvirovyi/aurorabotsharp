using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AuroraBotSharp.DB;
using Discord;
using Discord.Interactions;
using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;

namespace AuroraBotSharp;

public class BotHandlingService
{
    private readonly DiscordSocketClient discord;
    private readonly InteractionService _commands;
    private readonly IServiceProvider _services;
    private readonly ABDbContext db;

    public BotHandlingService(DiscordSocketClient discord, InteractionService commands, ABDbContext db, IServiceProvider services)
    {
        this.discord = discord;
        _commands = commands;
        _services = services;

        this.db = db;
    }

    public async Task InitializeAsync()
    {
        await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _services);

        discord.Ready += OnReadyAsync;
        //discord.InteractionCreated += HandleInteraction;
    }

    private async Task HandleInteraction(SocketInteraction arg)
    {
        try
        {
            // Create an execution context that matches the generic type parameter of your InteractionModuleBase<T> modules
            var ctx = new SocketInteractionContext(discord, arg);
            await _commands.ExecuteCommandAsync(ctx, _services);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);

            // If a Slash Command execution fails it is most likely that the original interaction acknowledgement will persist. It is a good idea to delete the original
            // response, or at least let the user know that something went wrong during the command execution.
            if (arg.Type == InteractionType.ApplicationCommand)
                await arg.GetOriginalResponseAsync().ContinueWith(async (msg) => await msg.Result.DeleteAsync());
        }
    }
    
    /// <summary>
    /// Actualizing info about saved roles of all server members.
    /// </summary>
    /// <param name="guild">Guild for actualizing member roles</param>
    public async Task SavedRolesUpdateServerAsync(SocketGuild guild)
    {
        await guild.DownloadUsersAsync();
        foreach (var user in guild.Users)
        {
            await SavedRolesUpdateMember(user);
        }
    }

    /// <summary>
    /// Actualizing info about saved roles of server member.
    /// </summary>
    /// <param name="user">Server member for actualizing roles</param>
    public async Task SavedRolesUpdateMember(SocketGuildUser user)
    {
        var savedRoles = await db.SavedRoles.AsQueryable().Where(sr => sr.ServerID == user.Guild.Id && sr.UserID == user.Id).ToListAsync();
            
        foreach (var savedRole in savedRoles)
        {
            if (user.Roles.FirstOrDefault(r => r.Id == savedRole.RoleID) == null)
            {
                db.SavedRoles.Remove(savedRole);
            }
        }

        foreach (var userRole in user.Roles)
        {
            if (!userRole.IsEveryone && savedRoles.FirstOrDefault(sr => sr.RoleID == userRole.Id) == null)
            {
                await db.SavedRoles.AddAsync(new()
                {
                    RoleID = userRole.Id,
                    ServerID = user.Guild.Id,
                    UserID = user.Id
                });
            }
        }

        await db.SaveChangesAsync();
    }

    /// <summary>
    /// Creating new voice channel for auto-voice feature.
    /// </summary>
    /// <param name="server">Server settings</param>
    /// <param name="guild">Guild to operate with</param>
    public async Task AutoVoiceCreateNewChannel(ABServer server, SocketGuild guild)
    {
        ABVoiceChannel voiceChannel = db.VoiceChannels.AsQueryable()
            .Where(vc => vc.ServerID == server.ID).OrderBy(vc => vc.Usages).First();
        await guild.CreateVoiceChannelAsync(voiceChannel.Name, properties =>
        {
            properties.CategoryId = server.AutoVoiceCategoryID;
            properties.Bitrate = guild.MaxBitrate;
        });
        voiceChannel.Usages = (ushort)(voiceChannel.Usages + 1);
        db.VoiceChannels.Update(voiceChannel);
        db.SaveChanges();
    }
        
    /// <summary>
    /// Bot initialization sequence.
    /// </summary>
    private async Task OnReadyAsync()
    {
        await OnAllGuildsAsync();
        
        discord.UserJoined += OnMemberAsync;
        discord.GuildMemberUpdated += OnMemberUpdateAsync;
        discord.UserVoiceStateUpdated += DiscordOnUserVoiceStateUpdated;
        discord.RoleDeleted += OnDeleteRole;
        discord.ChannelDestroyed += OnChannelDestroyed;
        //
        discord.InteractionCreated += HandleInteraction;
    }

    /// <summary>
    /// Handles channel deletion.
    /// </summary>
    /// <param name="channel">Deleted channel</param>
    public async Task OnChannelDestroyed(SocketChannel channel)
    {
        var guildChannel = channel as SocketGuildChannel;
        if (guildChannel != null)
        {
            var server = await db.Servers.FindAsync(guildChannel.Guild.Id);
            if (server.AutoVoiceCategoryID == guildChannel.Id)
            {
                await guildChannel.Guild.Owner.SendMessageAsync(
                    $"Auto-voice feature on {guildChannel.Guild.Name} has been disabled, because auto-voice category has been deleted.");
                server.AutoVoiceCategoryID = null;
                db.Servers.Update(server);
                db.SaveChanges();
            }
        }
    }

    /// <summary>
    /// Handling role deletion.
    /// </summary>
    /// <param name="role">Deleted role</param>
    private async Task OnDeleteRole(SocketRole role)
    {
        var server = await db.Servers.FindAsync(role.Guild.Id);
        if (server != null && server.SaveRoles)
        {
            db.SavedRoles.RemoveRange(db.SavedRoles.AsQueryable().Where(sr => sr.ServerID == server.ID && sr.RoleID == role.Id));
            if (server.AutoRoleID != null && server.AutoRoleID == role.Id)
            {
                await role.Guild.Owner.SendMessageAsync($"Auto-role feature on {role.Guild.Name} has been disabled, because specified role has been deleted.");
                server.AutoRoleID = null;
                db.Servers.Update(server);
            }
            db.SaveChanges();
        }
    }

    /// <summary>
    /// Handling user voice state change.
    /// </summary>
    /// <param name="user">User</param>
    /// <param name="oldState">Old voice state</param>
    /// <param name="newState">New voice state</param>
    private async Task DiscordOnUserVoiceStateUpdated(SocketUser user, SocketVoiceState oldState, SocketVoiceState newState)
    {
        var guildUser = user as SocketGuildUser;
        if (guildUser != null)
        {
            var server = await db.Servers.FindAsync(guildUser.Guild.Id);
            if (server.AutoVoiceCategoryID != null)
            {
                if (oldState.VoiceChannel != null && oldState.VoiceChannel.CategoryId == server.AutoVoiceCategoryID && oldState.VoiceChannel != newState.VoiceChannel && oldState.VoiceChannel.Users.Count == 0)
                {
                    var voiceChannel = db.VoiceChannels.Find(server.ID, oldState.VoiceChannel.Name);
                    voiceChannel.Usages = (ushort)(voiceChannel.Usages - 1);
                    db.VoiceChannels.Update(voiceChannel);
                    db.SaveChanges();
                    await oldState.VoiceChannel.DeleteAsync();
                }
                if (newState.VoiceChannel != null && newState.VoiceChannel.CategoryId == server.AutoVoiceCategoryID && newState.VoiceChannel != oldState.VoiceChannel && newState.VoiceChannel.Users.Count == 1)
                {
                    await AutoVoiceCreateNewChannel(server, guildUser.Guild);
                }
            }
        }
    }
    
    /// <summary>
    /// Actualizing info about all the connected guilds.
    /// </summary>
    public async Task OnAllGuildsAsync()
    {
        foreach (var guild in discord.Guilds)
        {
            await OnGuildAsync(guild);
        }
        db.Servers.RemoveRange(db.Servers.AsQueryable().Where(s => !discord.Guilds.Select(g => g.Id).Contains(s.ID)));
        db.SaveChanges();
    }
    
    /// <summary>
    /// Actualizing info about the guild.
    /// </summary>
    /// <param name="guild">Connected guild</param>
    public async Task OnGuildAsync(SocketGuild guild)
    {
        var server = await db.Servers.FindAsync(guild.Id);
        if (server == null)
        {
            db.Servers.Add(new() {
                ID = guild.Id,
                SaveRoles = false
            });
            await db.SaveChangesAsync();
        }
        else
        {
            if (server.SaveRoles)
            {
                if (server.AutoRoleID != null)
                {
                    await guild.DownloadUsersAsync();
                    if (guild.GetRole(server.AutoRoleID.Value) == null)
                    {
                        await guild.Owner.SendMessageAsync(
                            $"Auto-role feature on {guild.Name} has been disabled, because role id became wrong");
                        server.AutoRoleID = null;
                        db.Servers.Update(server);
                        await db.SaveChangesAsync();
                        
                        await SavedRolesUpdateServerAsync(guild);
                    }
                    else
                    {
                        foreach (var user in guild.Users)
                        {
                            var userSavedRoles = db.SavedRoles.AsQueryable().Where(sr =>
                                sr.ServerID == guild.Id && sr.UserID == user.Id).Select(sr => sr.RoleID).ToList();
                            if (user.Roles.Count == 1)
                            {
                                if (userSavedRoles.Count == 0)
                                {
                                    await user.AddRoleAsync(server.AutoRoleID.Value);
                                }
                                else
                                {
                                    await user.AddRolesAsync(userSavedRoles);
                                }
                            }
                            else
                            {
                                await SavedRolesUpdateMember(user);
                            }
                        }
                    }
                }
                else
                {
                    await SavedRolesUpdateServerAsync(guild);
                }
            }

            if (server.AutoVoiceCategoryID != null)
            {
                var autoVoiceCategory = guild.GetCategoryChannel(server.AutoVoiceCategoryID.Value);
                if (autoVoiceCategory == null)
                {
                    await guild.Owner.SendMessageAsync(
                        $"Auto-voice feature on {guild.Name} has been disabled, because auto-voice category id became wrong.");
                    server.AutoVoiceCategoryID = null;
                    db.Servers.Update(server);
                    db.SaveChanges();
                }
                else
                {
                    db.VoiceChannels.UpdateRange(db.VoiceChannels.AsQueryable().Where(vc => vc.ServerID == server.ID).Select(vc => new ABVoiceChannel
                    {
                        ServerID = vc.ServerID,
                        Name = vc.Name,
                        Usages = 0
                    }));
                    db.SaveChanges();
                    bool hasEmptyVoiceChannels = false;
                    foreach (var channel in autoVoiceCategory.Channels)
                    {
                        var dbChannel = db.VoiceChannels.Find(server.ID, channel.Name);
                        if (dbChannel != null)
                        {
                            if (channel.Users.Count == 0)
                            {
                                if (!hasEmptyVoiceChannels)
                                {
                                    hasEmptyVoiceChannels = true;
                                }
                                else
                                {
                                    await channel.DeleteAsync();
                                    continue;
                                }
                            }
                            dbChannel.Usages = (ushort)(dbChannel.Usages + 1);
                            db.VoiceChannels.Update(dbChannel);
                            db.SaveChanges();
                        }
                        else
                        {
                            await channel.DeleteAsync();
                        }
                    }
                    if (!hasEmptyVoiceChannels)
                    {
                        await AutoVoiceCreateNewChannel(server, guild);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Handling new guild member.
    /// </summary>
    /// <param name="user">New member</param>
    public async Task OnMemberAsync(SocketGuildUser user)
    {
        var server = await db.Servers.FindAsync(user.Guild.Id);
        if (server.SaveRoles)
        {
            var savedRoles = await db.SavedRoles.AsQueryable().Where(sr => sr.ServerID == user.Guild.Id && sr.UserID == user.Id).ToListAsync();
            
            foreach (var savedRole in savedRoles)
            {
                if (user.Roles.FirstOrDefault(r => r.Id == savedRole.RoleID) == null)
                {
                    await user.AddRoleAsync(savedRole.RoleID);
                }
            }
            
            if (savedRoles.Count == 0 && server.AutoRoleID != null)
            {
                await user.AddRoleAsync(server.AutoRoleID.Value);
            }
        }
    }

    /// <summary>
    /// Handling member update(roles, nickname and other).
    /// </summary>
    /// <param name="oldUser">Old member state</param>
    /// <param name="user">Current member state</param>
    public async Task OnMemberUpdateAsync(Cacheable<SocketGuildUser, ulong> oldUser, SocketGuildUser user)
    {
        var server = db.Servers.Find(user.Guild.Id);
        if (server.SaveRoles && !user.Roles.SequenceEqual((await oldUser.GetOrDownloadAsync()).Roles))
        {
            var userSavedRoles = db.SavedRoles.AsQueryable().Where(sr =>
                sr.ServerID == user.Guild.Id && sr.UserID == user.Id).Select(sr => sr.RoleID).ToList();
            if (server.AutoRoleID != null && user.Roles.Count == 1)
            {
                if (userSavedRoles.Count == 0)
                {
                    await user.AddRoleAsync(server.AutoRoleID.Value);
                }
                else
                {
                    await user.AddRolesAsync(userSavedRoles);
                }
            }
            else
            {
                await SavedRolesUpdateMember(user);
            }
        }
    }
}