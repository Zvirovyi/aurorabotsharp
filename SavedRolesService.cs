using Discord.WebSocket;

namespace AuroraBotSharp
{
    /// <summary>
    /// Service for providing UpdateServer command between BHS and Command service.
    /// </summary>
    public class SavedRolesService
    {
        public delegate void UpdateServerDelegate(SocketGuild guild);

        public UpdateServerDelegate UpdateServer;
    }
}