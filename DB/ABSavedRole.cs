using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace AuroraBotSharp.DB
{
    /// <summary>
    /// Saved user role at specified server.
    /// </summary>
    public class ABSavedRole
    {
        [ForeignKey("Server")]
        public ulong ServerID { get; set; }
        public ABServer Server { get; set; }
        
        public ulong UserID { get; set; }
        
        public ulong RoleID { get; set; }
    }
}