using System.ComponentModel.DataAnnotations.Schema;

namespace AuroraBotSharp.DB
{
    /// <summary>
    /// Server voice channel naming with usages.
    /// </summary>
    public class ABVoiceChannel
    {
        [ForeignKey("Server")]
        public ulong ServerID { get; set; }
        public ABServer Server { get; set; }
        
        public string Name { get; set; }
        
        public ushort Usages { get; set; }
    }
}