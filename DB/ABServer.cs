using System.ComponentModel.DataAnnotations;

namespace AuroraBotSharp.DB
{
    /// <summary>
    /// Server settings record.
    /// </summary>
    public class ABServer
    {
        [Key]
        public ulong ID { get; set; }
        
        public bool SaveRoles { get; set; }
        
        public ulong? AutoRoleID { get; set; }
        
        public ulong? AutoVoiceCategoryID { get; set; }
        
        public ulong? CustomCategoryID { get; set; }
    }
}