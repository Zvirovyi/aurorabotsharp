using Microsoft.EntityFrameworkCore;

namespace AuroraBotSharp.DB
{
    /// <summary>
    /// Database context for bot.
    /// </summary>
    public class ABDbContext : DbContext
    {
        public ABDbContext(DbContextOptions options) : base(options) { }
        
        public DbSet<ABServer> Servers { get; set; }
        
        public DbSet<ABSavedRole> SavedRoles { get; set; }
        
        public DbSet<ABVoiceChannel> VoiceChannels { get; set; }
        
        public DbSet<ABCustomChannel> CustomChannels { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ABSavedRole>().HasKey(r => new { r.RoleID, r.UserID, r.ServerID } );
            modelBuilder.Entity<ABVoiceChannel>().HasKey(vc => new { vc.ServerID, vc.Name });
            modelBuilder.Entity<ABCustomChannel>().HasKey(cc => new { cc.ServerID, cc.ID });
        }
    }
}